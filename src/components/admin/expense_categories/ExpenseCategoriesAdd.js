import Admin from "../Admin";
import React, { useRef,useState, useContext } from 'react'
import classes from './ExpenseCategoriesAdd.module.css'
import { Link, useHistory } from "react-router-dom";
import {useParams} from 'react-router'
import { useEffect } from "react/cjs/react.development";
import UserSession from '../../store/UserSession'


function ExpenseCategoriesAdd(){

    const appContext = useContext(UserSession)

    const [categoryRecordId, setcategoryRecordId] = useState(null)
    const refCategoryName = useRef(null)
    const refRecordId = useRef(null)

    const history = useHistory();
    const {id} = useParams()

    const recordId = id != null && id !== undefined ? id : null;


    //if we are editing gets the 
    //of the category we are editing
    async function getCategory(categoryId){

        const response = await fetch('http://localhost:8003/api/expense-category/'+categoryId)
        const jsonResp = await response.json()

        if(jsonResp.statusCode !== "0"){
            alert("Failed to get category details. " + jsonResp.statusDescription)
            return;
        }

        //successfully retrieved the records from 
        //the server, so we render
        const categoryRecord = jsonResp.result;
        document.getElementById("category").value = categoryRecord.category_name;
        setcategoryRecordId(jsonResp.id);

    }

    useEffect(function(){

        if(recordId != null){
            getCategory(recordId)
        }

    }, []);


    async function submitData(data, isEditMode){

        const url = isEditMode ? 'http://localhost:8003/api/expense-category/'+data.id : 
                                 'http://localhost:8003/api/expense-category';
        const method = isEditMode ? 'put' : 'post'
        
        const response = await fetch(url,{
            method : method,
            body: JSON.stringify(data),
            headers : {
                "Content-Type" : "application/json"
            }
        })

        const jsonResp = await response.json();
        if(jsonResp.statusCode !== "0"){
            alert(jsonResp.statusDescription)
            return;
        } 

        //redirect to expense-categories index
        appContext.openAlertDialog("Operation successful", "Category successfully saved");
      //  history.replace('/admin/expense-categories')

    }

    function saveCategoryHandler(event){
        event.preventDefault();

        let inputCategoryName = refCategoryName.current.value;
        if(inputCategoryName == null || inputCategoryName == ""){
            alert("Category is required");
            return;
        }

        //if we have the recordId, then we are editting a category
        //else we are just saving the category

        let reqData = { "category_name":inputCategoryName }
        if(recordId == null){
            submitData(reqData, false); 
        }else{
            let updated = {...reqData, 'id':recordId}
            submitData(updated, true); 
        }    

    }


    return (
        <Admin title={recordId == null ? "Add New Expense Category" : "Edit Expense Category"} >
            <form className={classes.form_container}>

                {recordId !== null ? <input type='hidden' value={categoryRecordId} name='record_id' ref={refRecordId} /> : ''}

                <div className={classes.form_group}>
                    <div><label >Category:</label></div>
                    <div><input id="category" ref={refCategoryName} className={classes.form_control} type="text"/></div>
                </div>
                <div className={classes.form_group}>
                    <Link className="btn btn-danger right ml-5" to="/admin/expense-categories">Cancel</Link>
                    <button className="btn btn-primary right" onClick={saveCategoryHandler}>Save Category</button>                    
                </div>

            </form>
        </Admin>
    );
}
export default ExpenseCategoriesAdd;
