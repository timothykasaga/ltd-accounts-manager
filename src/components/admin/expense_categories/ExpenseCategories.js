import { Link } from "react-router-dom";
import { useEffect, useState } from "react/cjs/react.development";
import Admin from "../Admin";
import { useHistory } from "react-router-dom";

function ExpenseCategories(){

    const history = useHistory();
    const [categories, setCategories] = useState([])

    async function removeCategory(categoryId){

        const response = await fetch('http://localhost:8003/api/expense-category/'+categoryId,{
            method:'delete',
        });
        
        const jsonResp = await response.json();
        if(jsonResp.statusCode != "0"){
            alert(jsonResp.statusDescription)
            return;
        }

        //successfully deleted
        //reload the categories
        getExpenseCategories();

    }

    function editCategoryHandler(event){
        let categoryId = event.target.attributes['data-category-id'].value;
        history.replace('/admin/expense-categories/edit/'+categoryId);
    }

    function removeCategoryHandler(event){
        let categoryId = event.target.attributes['data-category-id'].value;
        removeCategory(categoryId);

    }

    async function getExpenseCategories(){

        const response = await fetch('http://localhost:8003/api/expense-category')
        const json = await response.json();

        let statusCode = json.statusCode;
        let statusDesc = json.statusDesc;
        let results = json.result;

        if(statusCode !== "0"){
            alert(statusDesc);
            return;
        }

        setCategories(results);

    }

    useEffect(function(){
        getExpenseCategories();
    },[]);

    return (

        <Admin title="Expense Categories">
            <div>
                <div><Link to="/admin/expense-categories/add" className="btn btn-primary">Add Category</Link></div>
                <table className='table' border="1">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th className='td-width-5'></th>
                            <th className='td-width-5'></th>
                        </tr>
                    </thead>
                    <tbody>
                    {categories.length == 0 ? <tr><td colSpan='4' className='center'>No Records found</td></tr> : 
                     categories.map(category => 
                     <tr key={category.id}>
                         <td>{category.category_name}</td>
                         <td><button data-category-id={category.id} className='btn btn-primary' onClick={editCategoryHandler}>Edit</button></td>
                         <td><button data-category-id={category.id} className='btn btn-danger' onClick={removeCategoryHandler} >Remove</button></td>
                     </tr>
                     )
                    }
                    </tbody>
                </table>

            </div>
        </Admin>

    );
}

export default ExpenseCategories