
import { useRef, useState, useEffect } from 'react';
import classes from './ExpenseItemsAdd.module.css'
import Admin from '../Admin'
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';


const ExpenseItemsAdd = () => {

    const refItemCode = useRef(null);
    const refItemName = useRef(null);
    const refUnitCost = useRef(null);
    const refExpenseCategory = useRef(null);
    const history = useHistory();

    //the values to show in the categories drop down
    const [expenseCategories, setExpenseCategories] = useState([]);

    const getCategories = async () => {

        const response = await fetch('http://localhost:8003/api/expense-category');
        const jsonResp = await response.json();

        if(jsonResp.statusCode != "0"){
            return;  //failed to get the categories
        }

        setExpenseCategories(jsonResp.result);

    }

    const saveItem = async (item) =>{

        const response = await fetch('http://localhost:8003/api/expense-item',{
            method:'post',
            body: JSON.stringify(item),
            headers: {
                'Content-Type':'application/json'
            }
        })

        const jsonResp = await response.json();
        if(jsonResp.statusCode != "0"){
            const errorField = document.querySelector('.error');
            errorField.textContent = jsonResp.statusDescription;
            errorField.classList.add("bordered-error");
            return;
        }

        //successfully saved the data to the server
        //redirect to the items page
        history.replace('/admin/expense-items')

    }

    const validInput = () => {     

        //validate the form input
        //if no error return null
        //else return on first error
        
        if(refItemCode.current.value == ""){
            return "Item code is required"
        }
        if(refItemName.current.value == ""){
            return "Item name is required"
        }
        if(refUnitCost.current.value == ""){
            return "Unit cost is required"
        }
        if(refExpenseCategory.current.value == ""){
            return "Expense category is required"
        }
        return null;

    }

    const saveExpenseItemHandler = (event) => {

        event.preventDefault();

        //clear the error field
        const errorField = document.querySelector('.error');
        errorField.textContent = "";
        errorField.classList.remove("bordered-error");

        let validationError = validInput();
        if(validationError != null){
            //invalid input
            //set the error field values
            errorField.textContent = validationError;
            errorField.classList.add("bordered-error");
            return;
        }


        //valid input
        //proceeed to save the data to the server
        let item = {
            category_id : refExpenseCategory.current.value,
            item_code : refItemCode.current.value,
            name : refItemName.current.value,
            unit_cost : refUnitCost.current.value,
        }

        saveItem(item);

    };


    //load the expense categories on page load
    useEffect(() => {
       getCategories(); 
    }, [])


    return (

        <Admin title='Add Expense Item' >
        <form className={classes.form_container}>

            <span className='error'></span>

            <div className={classes.form_group}>
                <div><label >Item Code:</label></div>
                <div><input ref={refItemCode} className={classes.form_control} type="text"/></div>
            </div>
            <div className={classes.form_group}>
                <div><label >Item Name:</label></div>
                <div><input ref={refItemName} className={classes.form_control} type="text"/></div>
            </div>
            <div className={classes.form_group}>
                <div><label >Unit Cost:</label></div>
                <div><input ref={refUnitCost} className={classes.form_control} type="number"/></div>
            </div>
            <div className={classes.form_group}>
                <div><label >Expense Category:</label></div>
                <div>
                    <select ref={refExpenseCategory} className={classes.form_control}>
                        <option value="">Select Category</option>
                        {expenseCategories.map(category => {
                            return <option id={category.id} value={category.id}>{category.category_name}</option>
                        })}
                    </select>
                </div>
            </div>

            <div className={classes.form_group}>
                <Link className="btn btn-danger right ml-5" to="/admin/expense-items">Cancel</Link>
                <button className="btn btn-primary right" onClick={saveExpenseItemHandler}>Save Expense Item</button>                    
            </div>

        </form>
    </Admin>
    );
}

export default ExpenseItemsAdd