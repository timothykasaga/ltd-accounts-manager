import { Link } from "react-router-dom";
import { useEffect, useState } from "react/cjs/react.development";
import Admin from "../Admin";
import { useHistory } from "react-router-dom";

function ExpenseItems(){

    const history = useHistory();
    const [items, setItems] = useState([])

    async function removeCategory(itemId){

        const response = await fetch('http://localhost:8003/api/expense-item/'+itemId,{
            method:'delete',
        });
        
        const jsonResp = await response.json();
        if(jsonResp.statusCode != "0"){
            alert(jsonResp.statusDescription)
            return;
        }

        //successfully deleted
        //reload the items
        getExpenseItems();

    }

    function editItemHandler(event){
        let itemId = event.target.attributes['data-item-id'].value;
        history.replace('/admin/expense-items/edit/'+itemId);
    }

    function removeItemHandler(event){
        let ItemId = event.target.attributes['data-item-id'].value;
        removeCategory(ItemId);
    }

    async function getExpenseItems(){

        const response = await fetch('http://localhost:8003/api/expense-item')
        const json = await response.json();

        let statusCode = json.statusCode;
        let statusDesc = json.statusDesc;
        let results = json.result;

        if(statusCode !== "0"){
            alert(statusDesc);
            return;
        }

        setItems(results);

    }

    useEffect(function(){
        getExpenseItems();
    },[]);

    return (

        <Admin title="Expense Items">
            <div>
                <div><Link to="/admin/expense-items/add" className="btn btn-primary">Add Expense Item</Link></div>
                <table className='table' border="1">
                    <thead>
                        <tr>                            
                            <th>Item Code</th>
                            <th>Name</th>
                            <th>Unit Cost</th>
                            <th>Category</th>
                            <th className='td-width-5'></th>
                            <th className='td-width-5'></th>
                        </tr>
                    </thead>
                    <tbody>
                    {items.length == 0 ? <tr><td colSpan='6' className='center'>No Records found</td></tr> : 
                     items.map(item => 
                     <tr key={item.id}>
                         <td>{item.item_code}</td>
                         <td>{item.name}</td>
                         <td>{item.unit_cost}</td>
                         <td>{item.category.category_name}</td>
                         <td><button data-item-id={item.id} className='btn btn-primary' onClick={editItemHandler}>Edit</button></td>
                         <td><button data-item-id={item.id} className='btn btn-danger' onClick={removeItemHandler} >Remove</button></td>
                     </tr>
                     )
                    }
                    </tbody>
                </table>

            </div>
        </Admin>

    );
}

export default ExpenseItems