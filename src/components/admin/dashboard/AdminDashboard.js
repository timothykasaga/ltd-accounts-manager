import Admin from "../Admin";
import classes from './AdminDashboard.module.css'
import { useState,useEffect } from "react";

const AdminDashboard = () => {

    const [stats, setStats] = useState({
        count_expense_categories : 0,
        count_expense_items : 0,
    })


    const getStats = async () => {

        const response = await fetch('http://localhost:8003/api/admin-dashboard/stats');
        const jsonResp = await response.json();

        if(jsonResp.statusCode !== "0"){
            return; //error on getting the stats
        }
        setStats(jsonResp.result);

    };


    //get the application stats from the server
    useEffect(() => {
        getStats();
    }, [])

    
    return (
        <Admin title='Admin Dashboard'>
            <div>
                <div className={classes.stat_container}>
                    <span className={classes.stat_title}>Expense Categories</span>
                    <span className={classes.stat_value}>{stats.count_expense_categories}</span>
                </div>
                <div className={classes.stat_container}>
                    <span className={classes.stat_title}>Expense Items</span>
                    <span className={classes.stat_value}>{stats.count_expense_items}</span>
                </div>
            </div>
        </Admin>
    );
}

export default AdminDashboard