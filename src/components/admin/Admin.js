import { Link } from "react-router-dom";
import classes from "./Admin.module.css"

function Admin({children, title}){
    return (
        <div className={classes.admin_page_container}>
            <div className={classes.side_menu}>
                <ul>
                    <li><Link to="/admin">Admin Dashboard</Link></li>
                    <li><Link to="/admin/expense-categories">Expense Categories</Link></li>
                    <li><Link to="/admin/expense-items">Expense Items</Link></li>
                </ul>
            </div>
            <div className={classes.content}>
                <div className={classes.page_header}>
                    <h3>{title}</h3>
                </div>
                <div className={classes.page_content}>
                    {children}
                </div>
            </div>
            <div className={classes.clearfix}></div>
        </div>
       
    );
}

export default Admin;