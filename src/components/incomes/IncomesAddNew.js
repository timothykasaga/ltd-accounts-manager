
import classes from "../incomes/IncomesAddNew.module.css"
import React, { useRef } from 'react'
import {Link, useHistory} from 'react-router-dom'

function IncomeAddNew(){

    const history = useHistory();

    const refCategory = useRef(null)
    const refItem = useRef(null)
    const refAmount = useRef(null)
    const refDate = useRef(null)

    function saveIncomeHandler(event){
        event.preventDefault();
        console.log("Form submission clicked");

        let data = {
            category : refCategory.current.value,
            item : refItem.current.value,
            amount : refAmount.current.value,
            date : refDate.current.value,
        }
        
        let jsonData = JSON.stringify(data);
        submitFormData(jsonData);

    }

    async function submitFormData(jsonData){

        const response = await fetch("http://127.0.0.1:8003/api/incomes", {
            method : "POST",
            body : jsonData,
            headers : {
                "Content-Type" : "application/json"
            }
        })

        const json = await response.json();
        if(json == "OK"){
            //redirect to incomes
            history.replace('/incomes')
        }

    }


    return(

        <div className="container mt-20 mb-20">
            <div className={classes.header_underlined}>Add New Income</div>
            <form className={classes.form_container}>
                <div className={classes.form_group}>
                    <div><label >Category:</label></div>
                    <div><input ref={refCategory} className={classes.form_control} id="category" type="text"/></div>
                </div>

                <div className={classes.form_group}>
                    <div><label >Item:</label></div>
                    <div><input ref={refItem} className={classes.form_control} id="item" type="text"/></div>
                </div>

                <div className={classes.form_group}>
                    <div><label >Amount:</label></div>
                    <div><input ref={refAmount} className={classes.form_control} id="amount" type="text"/></div>
                </div>

                <div className={classes.form_group}>
                    <div><label >Date:</label></div>
                    <div><input ref={refDate} className={classes.form_control} id="amount" type="date"/></div>
                </div>

                <div className={classes.form_group}>
                    <Link  className="btn btn-danger right ml-5" to="/incomes">Cancel</Link>
                    <button className="btn btn-primary right" onClick={saveIncomeHandler}>Save Income</button>
                </div>

            </form>
        </div>

    );
}

export default IncomeAddNew;