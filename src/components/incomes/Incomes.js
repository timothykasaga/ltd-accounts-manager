import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

function Income(){

    const [records, setRecords] = useState([])
    const [processing, setProcessing] = useState(true)
    const [dataLoaded, setDataLoaded] = useState(false)

    //pagination btn states
    const [hrefPageFirst, setHrefPageFirst] = useState(null)
    const [hrefPageLast, setHrefPageLast] = useState(null)
    const [hrefPagePrevious, setHrefPagePrevious] = useState(null)
    const [hrefPageNext, setHrefPageNext] = useState(null)

    const [previousPageActive, setPreviousPageActive] = useState(null)
    const [nextPageActive, setNextPageActive] = useState(null)


    //handle pagination
    function paginationLinksHandler(event){
        event.preventDefault();
        getData(event.target.href);
    }

    //update paginationu urls
    function updatePaginationUrls(data){
        setHrefPageFirst(data.first_page_url)

        setHrefPagePrevious(data.prev_page_url)
        setPreviousPageActive((data.prev_page_url == null)?'deactive':'active')

        setHrefPageNext(data.next_page_url)
        setNextPageActive((data.next_page_url == null)?'deactive':'active')
        setHrefPageLast(data.last_page_url)
    }

    //function loades the incomes records
    //from the server
    async function getData(apiUrl){
    
        console.log("Starting to load Data")

        const response = await fetch(apiUrl != null ? apiUrl : "http://127.0.0.1:8003/api/incomes")
        const data = await response.json()

        console.log(data);
        
        setRecords(data.data)
        setProcessing(false);

        updatePaginationUrls(data);

    }

   useEffect(() =>{
       getData(null);
   } , [dataLoaded])


   //only trigger the call to useEffect is the data has not been loaded
   //if you don't do this the function will be repeatedly called
   if(!dataLoaded){
        setDataLoaded(true);
   }


    const myData = (processing == true) ? (
        <div>
            <div className="preloader">Please wait as we process your request...</div>
        </div>
    ) : 
    (
        <div>
        <div><Link to="/incomes-add" className="btn btn-primary">Add Income</Link></div>
        <div>
            <table className="table" border="1">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Item</th>
                        <th className="td-width-15">Amount</th>
                        <th className="td-width-15">Transaction Date</th>
                        <th className="td-width-15">Created</th>
                    </tr>
                </thead>
                <tbody>
                { records.length === 0 ? 
                    <tr><td className='center' colSpan='5'>No Records</td></tr>
                    :
                    records.map(income => {
                        return <tr key={income.id}>
                                    <td>{income.category}</td>
                                    <td>{income.item}</td>
                                    <td>{income.amount}</td>
                                    <td>{income.date}</td>
                                    <td>{income.created_at}</td>
                                </tr>
                    })
                }                        
                </tbody>
                {
                    records.length === 0 ? '' : 
                    <tfoot className='pagination-container'>
                        <a href={hrefPageFirst} className='btn btn-paginate' onClick={paginationLinksHandler}>{'<'}</a>
                        <a href={hrefPagePrevious} className={'btn btn-paginate '+previousPageActive+''} onClick={paginationLinksHandler}>{'<<'}</a>
                        <a href={hrefPageNext} className={'btn btn-paginate '+nextPageActive+''} onClick={paginationLinksHandler}>{'>>'}</a>
                        <a href={hrefPageLast} className='btn btn-paginate' onClick={paginationLinksHandler}>{'>'}</a>
                    </tfoot>
                }
               
            </table>
        </div>
    </div>
    );

    return ( myData );

}

export default Income;