
import { useContext } from "react";
import UserSession from "./store/UserSession";

function Dashboard({app_name}){

    const userSessionCtx = useContext(UserSession)
  
    return (
        <div className="dashboard-content">
            <div>Welcome to the {app_name} Incomes & Expenses Tracker. 
            You are currently logged as {userSessionCtx.getFullName()}. Age {userSessionCtx.age()}</div>
        </div>

    );
}

export default Dashboard;