
function Footer({app_name}){
    return(
        <div className="footer">
            Copyright @2021 - {app_name}
        </div>
    );
}

export default Footer;