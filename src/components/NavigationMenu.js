
import {Link} from "react-router-dom"

function NavigationMenu({app_name}){
    return (
        <div className="navbar">
            <span className="app-logo">{app_name}</span>
            <ul className="nav-links">
                <li><Link to="/">Dashboard</Link></li>
                <li><Link to="/incomes">Incomes</Link></li>
                <li><Link to="/expenses">Expenses</Link></li>
                <li><Link to="/admin">Administration</Link></li>
            </ul>
        </div>
    )
}

export default NavigationMenu;