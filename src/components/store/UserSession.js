
import { createContext, useState } from "react";
import AlertDialog from "./AlertDialog";

const UserSession = createContext({
    app_name : 'Leontymo Developers',
    first_name : 'Timothy',
    last_name : 'Kasaga',
    dob : '1993-12-09',
    getFullName : ()=>{},
    age : ()=>{},
    openAlertDialog : (title, message)=>{},
})

export function UserSessionProvider({children}){

    const [alertTitle, setAlertTitle] = useState("");
    const [alertMessage, setAlertMessage] = useState("");
    const [showAlert, setShowAlert] = useState(false);

    const context = {
        app_name : 'Anastasia',
        first_name : 'Timothy',
        last_name : 'Kasaga',
        dob : '1993-12-09',
        getFullName : fullName,
        age : getAge,
        openAlertDialog : openAlertDialogHandler
    }

    function openAlertDialogHandler(title, message){
        setShowAlert(false)
        setAlertTitle(title)
        setAlertMessage(message)
        setShowAlert(true)
    }

    function fullName(){
        return context.first_name + " " +context.last_name;
    }

    function getAge(){
        let now = new Date();
        let date_of_birth = new Date(context.dob);
        return now.getFullYear() - date_of_birth.getFullYear();
    }

    return(
        <UserSession.Provider value={context}>
            {children}
            <AlertDialog title={alertTitle} message={alertMessage} show={showAlert}></AlertDialog>
        </UserSession.Provider>
    );

}

export default UserSession;