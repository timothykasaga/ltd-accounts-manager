
import classes from './AlertDialog.module.css'

function AlertDialog({title, message, show}){

    console.log(show);

    show = show == null || show === undefined ? false : show;
    let defaultShowClass = '';
    if(!show){
        defaultShowClass = 'hide'
    }
    
    function closeBtnHandler(event){
        event.preventDefault();
        document.querySelector('#alert_dialog').classList.add('hide');
    }

    return (
        <div id='alert_dialog' className={defaultShowClass} >
            <div className={classes.alert_dialog}>
                <div className={classes.header}>{title}</div>
                <div className={classes.content}>{message}</div>
                <div className={classes.footer}>
                    <div className='right'>
                        <a href='!#' className='btn btn-danger' onClick={closeBtnHandler}>Close</a>
                    </div>
                </div>
            </div>            
        </div>
        
    );

}

export default AlertDialog
