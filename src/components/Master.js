
import NavigationMenu from "./NavigationMenu"
import Footer from "./Footer"

function Master({children, app_name}){
    return (
        <div>
            <NavigationMenu app_name={app_name}/>
            <div className="container">
                {children}
            </div>
            <Footer app_name={app_name}/>
        </div>
    );
}

export default Master;