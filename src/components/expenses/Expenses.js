
import {useState ,useEffect} from 'react'
import {Link, useHistory} from 'react-router-dom'

const Expenses = () => {

    const [expenses, setExpenses] = useState([])
    const history = useHistory();

    const getExpenses = async () => {

        const response = await fetch('http://localhost:8003/api/expense',{
            headers :{
                "Accept": "application/json",
                "Authorization": "Bearer FTC1Z0rjHGnbyOiM4qN3EYi8nxO1CR2Bcu5D75ZRnf1BFDFPqhXMWARCtMxAvvZKNak5n1QaF4oKmbZh"
            }
        });

        let okHttpStatus = response.status >= 200 && response.status <= 299;
        if (!okHttpStatus) {
             let error = response.status + ": "+response.statusText;
             alert(error);
             return;
        } 

        const jsonResp = await response.json();

        if(jsonResp.statusCode !=="0"){
            alert(jsonResp.statusDescription);
            return;
        }

        setExpenses(jsonResp.result);

    }
    
    function editExpenseHandler(event){
        let expenseId = event.target.attributes['data-expense-id'].value;
        history.replace('/expenses-edit/'+expenseId);
    }

    function removeExpenseHandler(event){
        let expenseId = event.target.attributes['data-expense-id'].value;
        removeExpense(expenseId);
    }

    async function removeExpense(itemId){

        const response = await fetch('http://localhost:8003/api/expense/'+itemId,{
            method:'delete',
        });
        
        const jsonResp = await response.json();
        if(jsonResp.statusCode !== "0"){
            alert(jsonResp.statusDescription)
            return;
        }

        //successfully deleted
        //reload the records
        getExpenses();

    }

    //load the expenses from the server on page load
    useEffect(() => {
        getExpenses();
    }, [])


    return (
        <div>
            <div><Link to="/expenses-add" className="btn btn-primary">Add Expense</Link></div>
            <div>
                <table className='table' border="1">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Unit Cost</th>
                            <th>Qauntity</th>
                            <th>Amount before Discount</th>
                            <th>Discount</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Justification</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {expenses.length === 0 ?
                        <tr><td colSpan='10' className='center'>No Records Found</td></tr>
                        :
                        expenses.map(expense => {
                            return <tr key={expense.id}>
                                <td>{expense.item.name}</td>
                                <td>{expense.unit_cost}</td>
                                <td>{expense.quantity}</td>
                                <td>{expense.amount_before_discount}</td>
                                <td>{expense.discount}</td>
                                <td>{expense.amount}</td>
                                <td>{expense.expense_date}</td>
                                <td>{expense.justification}</td>
                                <td><button data-expense-id={expense.id} className='btn btn-primary' onClick={editExpenseHandler}>Edit</button></td>
                                <td><button data-expense-id={expense.id} className='btn btn-danger' onClick={removeExpenseHandler} >Remove</button></td>
                            </tr>
                        })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Expenses;