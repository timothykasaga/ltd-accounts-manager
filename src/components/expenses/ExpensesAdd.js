
import {useRef, useEffect, useState} from 'react'
import classes from './ExpensesAdd.module.css'
import { Link, useHistory, useParams } from 'react-router-dom';

const apiBaseUrl = 'http://localhost:8003/api/'

const ExpensesAdd = () => {

    const [expenseItems, setExpenseItems] = useState([]);
    const history = useHistory();

    const {id} = useParams()
    const recordId = id != null && id !== undefined ? id : null;
    
    const refRecordId = useRef(null)
    const refItem = useRef(null)
    const refUnitCost = useRef(0)
    const refQty = useRef(0)
    const refAmountBeforeDiscount = useRef(0)
    const refDiscount = useRef(0)
    const refAmount = useRef(0)
    const refDate = useRef(null)
    const refJustification = useRef(null)

    const amountBeforeDiscountCalcHandler = () => {
        const unitCost = refUnitCost.current.value;
        const qty = refQty.current.value;
        const amount = unitCost * qty;
        document.querySelector('#amount_before_discount').value = amount;
        amountCalcHandler();
    }

    const amountCalcHandler = () => {
        const amountBeforeDiscount = refAmountBeforeDiscount.current.value;
        const discount = refDiscount.current.value;
        const amount = amountBeforeDiscount - discount;
        document.querySelector('#amount').value = amount;
    }

    const itemChangeHandler = (event) => {

        let selectedValue = event.target.value;
        let unitCost = 0;

        //get selected option item 
        //and then get it's unit cost
        const options = event.target.children;
        for(let i =1; i<options.length; i++){           
           if(options[i].value === selectedValue){
              unitCost = options[i].attributes["data-unit-cost"].value;
           }
        }

        //fill in the unit cost value
        document.querySelector('#unit_cost').value = unitCost;
        amountBeforeDiscountCalcHandler();

    }

    const getExpenseItems = async () => {
        const response = await fetch(apiBaseUrl+'expense-item');
        const jsonResp = await response.json();
        if(jsonResp.statusCode !== "0"){
            alert(jsonResp.statusDescription)
            return
        }
        setExpenseItems(jsonResp.result);
    }

    const saveExpenseHandler = (event) => {

        event.preventDefault();

        //todo add validation logic
        
        let expense_item_id = refItem.current.value;
        let unit_cost = refUnitCost.current.value;
        let quantity = refQty.current.value;
        let amount_before_discount = refAmountBeforeDiscount.current.value;
        let discount = refDiscount.current.value;
        let amount = refAmount.current.value;
        let expense_date = refDate.current.value;
        let justification = refJustification.current.value;

        const expense = {
            expense_item_id : expense_item_id,
            unit_cost : unit_cost,
            quantity : quantity,
            amount_before_discount : amount_before_discount,
            discount : discount,
            amount : amount,
            expense_date : expense_date,
            justification : justification,
        }

        if(recordId === null){
            saveExpense(expense, false);
        }else{
            saveExpense(expense, true);
        }

    }

    const saveExpense = async (expense, isEdit) => {

        const url = isEdit ? (apiBaseUrl+'expense/'+recordId) : (apiBaseUrl+'expense'); 
        const mtd = isEdit ? 'put' : 'post'

        const response = await fetch(url,{
            method : mtd,
            body : JSON.stringify(expense),
            headers : {
                'Content-Type' : 'application/json'
            }
        })

        const jsonResp = await response.json();
        if(jsonResp.statusCode !== "0"){
            alert(jsonResp.statusDescription)
            return;
        }

        history.replace('/expenses');

    }

    const getExpenseToEdit = async (recordId) => {

        const response = await fetch(apiBaseUrl+'expense/'+recordId);
        const jsonResp = await response.json();
        if(jsonResp.statusCode !== "0"){
            alert(jsonResp.statusDescription)
            return;
        }

        const expense = jsonResp.result;
        document.querySelector('#item').value = expense.expense_item_id;
        document.querySelector('#unit_cost').value = expense.unit_cost;
        document.querySelector('#qty').value = expense.quantity;
        document.querySelector('#amount_before_discount').value = expense.amount_before_discount;
        document.querySelector('#discount').value = expense.discount;
        document.querySelector('#amount').value = expense.amount;
        document.querySelector('#date').value = expense.expense_date;
        document.querySelector('#justification').value = expense.justification;

    }
    

    //load the expense items from the server
    useEffect(()=>{
        getExpenseItems();        
    },[]);


    //we are editing an we need to 
    //fetch the record to edit
    useEffect(function(){
        if(recordId != null){
            getExpenseToEdit(recordId)
        }
    }, []);


    return (
        <div className="container mt-20 mb-20">
        <div className={classes.header_underlined}>{recordId !== null ? 'Edit Expense' : 'Add Expense'}</div>
        <form className={classes.form_container}>

            {recordId !== null ? <input ref={refRecordId} type='hidden' value={recordId}/> : ''}
            
            <div className={classes.form_group}>
                <div><label >Item:</label></div>
                <div>
                    <select ref={refItem} className={classes.form_control} id="item" onChange={itemChangeHandler}>
                        <option value=''>Select Item</option>
                        {expenseItems.map(item => {
                            return <option 
                            key={item.id} 
                            value={item.id}
                            data-unit-cost={item.unit_cost}
                            >{item.name}</option>
                        })}
                    </select>
                </div>
            </div>

            <div className={classes.form_group}>
                <div><label >Unit Cost:</label></div>
                <div><input readOnly ref={refUnitCost} className={classes.form_control} id="unit_cost" type="text"/></div>
            </div>

            <div className={classes.form_group}>
                <div><label >Quantity:</label></div>
                <div><input ref={refQty} className={classes.form_control} id="qty" type="number" onChange={amountBeforeDiscountCalcHandler}/></div>
            </div>

            <div className={classes.form_group}>
                <div><label >Amount before Discount:</label></div>
                <div><input readOnly ref={refAmountBeforeDiscount} className={classes.form_control} id="amount_before_discount" type="number"/></div>
            </div>

            <div className={classes.form_group}>
                <div><label >Discount:</label></div>
                <div><input ref={refDiscount} className={classes.form_control} id="discount" type="number" onChange={amountCalcHandler}/></div>
            </div>

            <div className={classes.form_group}>
                <div><label >Amount:</label></div>
                <div><input readOnly ref={refAmount} className={classes.form_control} id="amount" type="number"/></div>
            </div>

            <div className={classes.form_group}>
                <div><label >Date:</label></div>
                <div><input ref={refDate} className={classes.form_control} id="date" type="date"/></div>
            </div>

            <div className={classes.form_group}>
                <div><label >Justification:</label></div>
                <div>
                    <textarea rows='2' ref={refJustification} className={classes.form_control} id="justification"></textarea>
                </div>
            </div>

            <div className={classes.form_group}>
                <Link  className="btn btn-danger right ml-5" to="/expenses">Cancel</Link>
                <button className="btn btn-primary right" onClick={saveExpenseHandler}>Save Expense</button>
            </div>

        </form>
    </div>
    );
}

export default ExpensesAdd;