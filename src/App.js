
import Master from './components/Master';
import Dashboard from './components/Dashboard';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Income from './components/incomes/Incomes';
import IncomesAddNew from './components/incomes/IncomesAddNew';
import ExpenseCategories from './components/admin/expense_categories/ExpenseCategories';
import ExpenseCategoriesAdd from './components/admin/expense_categories/ExpenseCategoriesAdd';
import ExpenseItems from './components/admin/expense_items/ExpenseItems';
import ExpenseItemsAdd from './components/admin/expense_items/ExpenseItemsAdd';
import AdminDashboard from './components/admin/dashboard/AdminDashboard';
import Expenses from './components/expenses/Expenses';
import ExpensesAdd from './components/expenses/ExpensesAdd';
import { useContext } from 'react';
import UserSession from './components/store/UserSession';

function App() {


  const userSessionCtx = useContext(UserSession)
  const appName = userSessionCtx.app_name

  return (

    <BrowserRouter>

      <Switch>

        {/* Incomes Routes  */}
        <Route path="/incomes-add">
          <Master app_name={appName}>
            <IncomesAddNew />
          </Master>
        </Route>
        <Route path="/incomes">
          <Master app_name={appName}>
            <Income app_name={appName} />
          </Master>
        </Route>


        {/* Expenses Routes */}
        <Route exact path="/expenses">
          <Master app_name={appName}>
            <Expenses app_name={appName} />
          </Master>
        </Route>
        <Route exact path="/expenses-add">
          <Master app_name={appName}>
            <ExpensesAdd app_name={appName} />
          </Master>
        </Route>
        <Route exact path="/expenses-edit/:id">
          <Master app_name={appName}>
            <ExpensesAdd app_name={appName}/>
          </Master>
        </Route>


        {/* Admin Routes  */}
        <Route exact path="/admin/expense-categories/add">
          <Master app_name={appName}>
            <ExpenseCategoriesAdd app_name={appName}/>
          </Master>
        </Route>
        <Route exact path="/admin/expense-categories/edit/:id">
          <Master app_name={appName}>
            <ExpenseCategoriesAdd app_name={appName}/>
          </Master>
        </Route>
        <Route path="/admin/expense-categories">
          <Master app_name={appName}>
            <ExpenseCategories app_name={appName} />
          </Master>
        </Route>

        <Route exact path="/admin/expense-items/add">
          <Master app_name={appName}>
            <ExpenseItemsAdd app_name={appName} />
          </Master>
        </Route>
        <Route exact path="/admin/expense-items">
          <Master app_name={appName}>
            <ExpenseItems app_name={appName} />
          </Master>
        </Route>
        <Route path="/admin">
          <Master app_name={appName}>
            <AdminDashboard app_name={appName}/>
          </Master>
        </Route>


        {/* Home Page Route */}
        <Route path="/">
          <Master app_name={appName}>
            <Dashboard app_name={appName} />
          </Master>
        </Route>
      </Switch>

    </BrowserRouter>

  );
}

export default App;
